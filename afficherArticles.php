<?php
session_start();
?>

<!DOCTYPE html>
    <html lang="fr">

    <head>
        <meta charset="iso-8859-15">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cuisine</title>
        <link rel="stylesheet" href="style.css">
        <link rel="icon" type="image/png" href="image/portrait.jpg">
        <meta name="viewport" content="width=device-width" />


    </head>
    <h1>Les recettes de cuisine</h1>

<?php



try {
    $dsn = 'mysql:host=localhost;dbname=recettes';
    $user = 'dev1';
    $password = 'azerty';
    
    $recettes = [];
    
    $dbh = new PDO($dsn, $user, $password);
    

    
    $statement = $dbh->prepare("SELECT recette, ingredient, description_recette  FROM recette, ingredient WHERE recette.id = ingredient.id");
    $statement->execute();
   
    

    $recettes = $statement->fetchAll(PDO::FETCH_ASSOC);

} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}



?>






<?php if (empty( $recettes ) === false): ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Nom de la recette </th>
            <th scope="col">Nom de l'ingredient </th>
            <th scope="col">Description de la recette</th>
                       

        </tr>
        </thead>
        <tbody>
        <?php foreach ($recettes as $recette): ?>
            <tr>
                <th scope="row"><?php echo $recette['recette']; ?></th>
                <td><?php echo htmlspecialchars($recette['ingredient']); ?></td>
                <td><?php echo htmlspecialchars($recette['description_recette']); ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
   
<?php else: ?>
    <p>
        Aucune donnée enregistrée.
    </p>
<?php endif; ?>

<button><a href="AjouterArticle.php">retour</a></button>

<?php
$_SESSION["html"];

?>
<br>
<button><a href="recette.pdf" target="_blank" download="recette.pdf">Exporter recette</a></button>    


</html>





