# Recette

## Installation

A little intro about the installation.
```
$ git clone https://gitlab.com/louisnebout34/recette.git

## Description

Il s'agit d'un site permettant de créer des recettes de cuisine.
Vous pouvons y voir afficher le nom de la recette, la description de la recette et les ingrédients. 
Vous pouvez exporter la recette au format texte si vous le souhaitez.
